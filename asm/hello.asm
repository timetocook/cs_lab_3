.data:
    h 'Hello'
    w ', World!'
.text:
    .hello:
        pprt h
        je .world
        jmp .hello
    .world:
        pprt w
        je .hlt
        jmp .world
    .hlt:
        hlt